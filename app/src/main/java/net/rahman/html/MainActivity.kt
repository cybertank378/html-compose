package net.rahman.html

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import net.rahman.html.ui.theme.HtmlTheme

const val HTML_TEXT =
    """
<p>Mengikuti Masa Bayar Premi BCA Life Heritage Protection dengan ketentuan :</p><ol><li>Maksimum usia Tertanggung Tambahan hingga 75 tahun untuk manfaat Meninggal Dunia</li><li>Maksimum usia Tertanggung Tambahan hingga 65 tahun untuk manfaat ket</li></ol
    """

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            HtmlTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    Column(
                        modifier = Modifier
                            .verticalScroll(rememberScrollState())
                            .fillMaxSize()
                    ) {
                        Html(HTML_TEXT)
                    }
                }
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    HtmlTheme {
        Column(modifier = Modifier.verticalScroll(rememberScrollState())) {
            Html(HTML_TEXT)
        }
    }
}